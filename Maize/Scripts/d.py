#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__      = "Ricardo Ribeiro"
__credits__     = ["Ricardo Ribeiro"]
__license__     = "MIT"
__version__     = "0.0"
__maintainer__  = "Ricardo Ribeiro"
__email__       = "ricardojvr@gmail.com"
__status__      = "Development"


from __init__ import *


class SimpleExample(BaseWidget):


    def __init__(self):
        super(SimpleExample,self).__init__('Simple example')
        self._btn = ControlButton('click')
        self._btn.value = self.__btnAction


        #Definition of the forms fields
        self._control 	= ControlList('List')
        self._ck = ControlCheckBox("Months")

        self._yearL1     = ControlCheckBoxList("SCF")   # Entry 3
        self._yearL1.value = [('J1', False), ('F1', False),  ('M1', False),  ('A1', False), ('M1', False), \
             ('J1', False), ('J1', False), ('A1', False), ('S1', False), ('O1', False), ('N1', False), ('D1', False) ]
        
        self.formset = [' ',(' ', '_yearL1', ' '), '_btn', ' ']


    def __btnAction(self):
        lis = 12*[0]

        for i in range(len(self._yearL1.items)):
            tup = self._yearL1.items[i]
            if tup[1] == True:
                lis[i] = 1

        return lis



##################################################################################################################
##################################################################################################################
##################################################################################################################

#Execute the application
if __name__ == "__main__":	 pyforms.start_app( SimpleExample )
	